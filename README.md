# Kosmo_panel

KiCad footprint library for Kosmo panels

## Footprints

* Kosmo_DP3T_Slide_Switch_Hole
* Kosmo_Encoder_Hole
* Kosmo_Jack_Hole
* Kosmo_Jack_Horizontal_Hole
* Kosmo_LED_Hole
* Kosmo_Little_Jack_Hole
* Kosmo_OLED_128x64_0.96in_Hole
* Kosmo_OLED_128x64_1.3in_Hole
* Kosmo_Panel_Dual_Mounting_Holes
* Kosmo_Panel_Dual_Slotted_Mounting_Holes
* Kosmo_Panel_Mounting_Hole
* Kosmo_Panel_Slotted_Mounting_Hole
* Kosmo_Pot_Hole
* Kosmo_Push_Button_Daier_Hole
* Kosmo_Rotary_Switch_Hole
* Kosmo_Slide_pot_Alpha_60mm_Hole
* Kosmo_Switch_Hole
* Kosmo_Tact_Button_13mm_Square_Hole
* Kosmo_Trimmer_Pot_Hole

## Note
As of release v1.1 KiCad files are in KiCad 6 format. If you are using KiCad 5 you must use the v1.0 release.

Kosmo panel templates have been split off to [https://gitlab.com/rsholmes/kosmo_panel_templates](https://gitlab.com/rsholmes/kosmo_panel_templates).
